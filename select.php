<?php
// Connecting, selecting database
$dbconn = pg_connect("host=115.42.252.42 port=5433 dbname=kcc user=postgres password=admin")
    or die('Could not connect: ' . pg_last_error());

$query = $_GET['sql'];
$result = pg_query($query) or die('Query failed: ' . pg_last_error());

// Printing results in HTML
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
   
    foreach ($line as $val) {
        echo $val;
    }
   
}

// Free resultset
pg_free_result($result);

// Closing connection
pg_close($dbconn);
?>