<?php

$type = 'audio';
$id = $_GET['id'];

$prefix = $id.'_'.$type;

$path = 'C:\\wamp64\\www\\store';

$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
$data = array();
foreach($files as $file) {
    $time = DateTime::createFromFormat('U', filemtime($file->getPathname()));
    // no need to explode the time, just make it a datetime object
    if( $time > new DateTime('2014-01-15')) { // is PHP and is greater than jan 15 2014
        $data[] = array('filename' =>str_replace($path.'\\','',$file->getPathname())  , 'size' =>$file->getSize() ,'time' => $time->getTimestamp()); // push inside
    }
}
usort($data, function($a, $b){ // sort by time latest
    return $b['time'] - $a['time'];
});

$s = '';

foreach ($data as $key => $value) {
if ($value['size'] > 0 && strpos($value['filename'], $prefix) > -1 && strpos($value['filename'], '.mp4') > -1)
 {
    if (strlen($s) > 0)
$s = $s.',';

    $s = $s.'{"f1":"'.$value['filename'].'"}';
    }
}

$s = '{"rows" : ['.$s.']}';
echo $s;

?>